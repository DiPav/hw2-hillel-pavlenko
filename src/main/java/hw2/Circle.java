package hw2;

import java.util.Arrays;

public class Circle {
    private final Point center;
    private final double radius;

    public Circle(Point center, double radius) {
        this.center = center;
        this.radius = radius;
    }

    public boolean checkPoint(Point usersPoint) {
        double x1 = center.getX();
        double x2 = usersPoint.getX();
        double y1 = center.getY();
        double y2 = usersPoint.getY();

        return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)) < radius;
    }

    public Point[] checkPoints(Point[] usersPoints) {
        Point[] pointsInCircle = new Point[]{};
        for (Point usersPoint : usersPoints) {
            if (checkPoint(usersPoint)) {
                pointsInCircle = appendPointToArray(pointsInCircle, usersPoint);
            }
        }
        return pointsInCircle;
    }

    private static Point[] appendPointToArray(Point[] array, Point value) {
        Point[] result = Arrays.copyOf(array, array.length + 1);
        result[result.length - 1] = value;
        return result;
    }
}
