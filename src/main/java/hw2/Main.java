package hw2;

import java.util.Scanner;

public class Main {
    static final int NUMBER_OF_USERS_POINTS = 10;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Ui ui = new Ui(scanner);
        //Створення масиву точок, введених користувачем та його ініціалізація за допомогою методу getUserPoints в Ui.java
        Point[] usersPoints = ui.getUsersPoints();
        //Створення кола, параметри якого(center, radius) дізнаємося за допомогою методів getCenter і getRadius в Ui.java
        Circle circle = new Circle(ui.getCenter(), ui.getRadius());
        //Перевірка точок в раніше створеному масиві за допомогою методу checkPoints в Circle.java,
        // повернення цим методом нового массиву лише з точками, які входять у округлість та
        // виведення їх у консоль за допомогою методу showPointsInCircle в Ui.java
        ui.showPointsInCircle(circle.checkPoints(usersPoints));
        //авто рестарт
        ui.restart();

    }
}
