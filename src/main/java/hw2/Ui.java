package hw2;

import java.util.Arrays;
import java.util.Scanner;

import static hw2.Main.NUMBER_OF_USERS_POINTS;
import static hw2.Main.main;

public class Ui {
    private final Scanner scanner;

    public Ui(Scanner scanner) {
        this.scanner = scanner;
    }

    public  Point[] getUsersPoints() {
        System.out.println("Hello! Enter coordinates of your points: ");
        Point[] arrayOfUserPoints = new Point[NUMBER_OF_USERS_POINTS];
        for (int i = 0; i < arrayOfUserPoints.length; i++) {
            System.out.print(i + 1 + ": ");

            arrayOfUserPoints[i] = transformInputToPoint(scanner.nextLine());
        }
        return arrayOfUserPoints;
    }

    public double getRadius() {
        System.out.print("Enter radius of circle: ");
        return scanner.nextDouble();
    }

    public Point getCenter() {
        System.out.print("Enter the coordinates of center: ");
        return transformInputToPoint(scanner.nextLine());
    }

    private Point transformInputToPoint(String line) {
        try {
            double[] tempDoubleArray = Arrays.stream(Arrays.stream(line
                            .replaceAll(" ", "")
                            .split(","))
                    .mapToDouble(Double::parseDouble)
                    .toArray()).toArray();
            if (tempDoubleArray.length < 2) {
                tempDoubleArray = Arrays.copyOf(tempDoubleArray, tempDoubleArray.length + 1);
                tempDoubleArray[tempDoubleArray.length - 1] = 0;
            }
            return new Point(tempDoubleArray[0], tempDoubleArray[1]);
        } catch (Exception e) {
            return new Point(0, 0);
        }
    }

    public void showPointsInCircle(Point[] pointsInCircle) {
        System.out.println("Points that are in circle: ");
        for (Point point : pointsInCircle) {
            System.out.println(point.toString());
        }
    }

    public void restart() {
        System.out.println("=============================");
        main(null);
    }
}
