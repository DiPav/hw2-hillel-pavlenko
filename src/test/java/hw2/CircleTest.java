package hw2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CircleTest {

    @Test
    void shouldDeleteBadPointsFromArray() {
        Circle circle = new Circle(new Point(0, 0), 5);
        Point point1 = new Point(10, 10);
        Point point2 = new Point(4, 4);
        Point[] arrayOfPoints = new Point[]{point1, point2};
        assertTrue(circle.checkPoints(arrayOfPoints).length < arrayOfPoints.length);
    }

    @Test
    void shouldReturnTrueIfPointInCircle() {
        Circle circle = new Circle(new Point(0,0),4);
        Point point = new Point(3,2);
        assertTrue(circle.checkPoint(point));
    }
    void shouldReturnFalseIfPointIsNotInCircle() {
        Circle circle = new Circle(new Point(0,0),4);
        Point point = new Point(5,5);
        assertFalse(circle.checkPoint(point));
    }
}