package hw2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PointTest {

    @Test
    void shouldReturnX() {
        Point point = new Point(2,2);
        assertEquals(2,point.getX());
    }

    @Test
    void shouldSetX() {
        Point point = new Point(2,2);
        point.setX(4);
        assertEquals(4,point.getX());
    }

    @Test
    void shouldReturnY() {
        Point point = new Point(2,2);
        assertEquals(2,point.getY());
    }

    @Test
    void shouldSetY() {
        Point point = new Point(2,2);
        point.setY(4);
        assertEquals(4,point.getY());
    }

    @Test
    void shouldReturnStringInfo() {
        Point point = new Point(2,2);
        assertEquals("Point{" + "x=" + point.getX() + ", y=" + point.getY() + '}',point.toString());
    }
}